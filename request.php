<?php

ini_set('display_errors', 0);
require_once 'library/Requests.php';
Requests::register_autoloader();

$headers = array(
	'Accept' => 'application/json, text/javascript, */*; q=0.01',
	'Accept-Encoding' => 'gzip, deflate',
	'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
	'Cache-Control' => 'no-cache',
	'Connection' => 'keep-alive',
	'host' => 'ot76.ru',
	'Pragma' => 'no-cache',
	'Referer' => 'http://ot76.ru/',
	'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36',
	'X-Requested-With' => 'XMLHttpRequest'
);


if($_POST == []){
	$_POST = json_decode(file_get_contents('php://input'), true);
}


switch ($_POST['action']) {
	case 'getallroutes':{// http://ot76.ru/getpe.php?vt=2&r=[%228%22]
		$request = Requests::get("http://ot76.ru/getpe.php?vt=" . $_POST['type'] . "&r=" . json_encode($_POST['bort']), $headers, array());
		break;
	}
	case 'getroutestations':{// http://ot76.ru/getpe.php?vt=2&r=[%228%22]
		$request = Requests::get("http://ot76.ru/getstations.php?vt=" . $_POST['type'] . "&r=" . json_encode($_POST['bort']), $headers, array());
		break;
	}
	case 'getroutecurve':{// http://ot76.ru/getpe.php?vt=2&r=[%228%22]
		$request = Requests::get("http://ot76.ru/getroute.php?vt=" . $_POST['type'] . "&r=" . $_POST['bort'], $headers, array());
		break;
	}
	
	default:
		# code...
		break;
}
echo $request->body;
?>