$('.group .list').slideToggle();
$('.group .name').click(function(event) {
  $(this).parent().toggleClass('active');
  $(this).parent().find('.list').slideToggle(400);
});

$('body').addClass('canvend');


var MAP_OBJ = [];

$.post('parse.php', {  }, 
  function(result)
    {

      var data = JSON.parse(result);

      console.log(data);

      data.autobus.map(function(elem, index) {
        $('.group-autobus .list').append('<div class="item" attrid="' + elem.id + '" attrcat="1"><div class="num">' + elem.text_id + '</div><div class="route"><span class="f">' + elem.from + '</span><span class="l">' + elem.to + '</span></div></div>');
      });

      data.trallbus.map(function(elem, index) {
        $('.group-trallbus .list').append('<div class="item" attrid="' + elem.text_id + '" attrcat="2"><div class="num">' + elem.text_id + '</div><div class="route"><span class="f">' + elem.from + '</span><span class="l">' + elem.to + '</span></div></div>');
      });

      data.tramv.map(function(elem, index) {
        $('.group-tramv .list').append('<div class="item" attrid="' + elem.text_id + '" attrcat="3"><div class="num">' + elem.text_id + '</div><div class="route"><span class="f">' + elem.from + '</span><span class="l">' + elem.to + '</span></div></div>');
      });

      data.marsh.map(function(elem, index) {
        $('.group-marsh .list').append('<div class="item" attrid="' + elem.id + '" attrcat="4"><div class="num">' + elem.text_id + '</div><div class="route"><span class="f">' + elem.from + '</span><span class="l">' + elem.to + '</span></div></div>');
      });


      $('.group .item').click(function(event) {
        $(this).toggleClass('active');
      });

      $('#searchfield').attr({
        disabled: false
      });

      $('#searchfield').quicksearch('.group .item', {
  
  'show': function () {
    if($('#searchfield').val().length!=0){
              $(this).parent().show();
            }

            $(this).removeClass('hided-search');
            this.style.display = "";
  },
  'hide': function () {
    $(this).addClass('hided-search');
    this.style.display = "none";
  }
});


      $('.getMap').click(function(event) {
        $('.group .item.active').each(function (elem) {
          MAP_OBJ.push({id: $(this).attr('attrid'), cat: $(this).attr('attrcat')});
        });



        $('.overlay-bl').hide(400);
        render();




      });


      });
// var term = '';
// function dosearch() {
//   term = $('#searchfield').val();
//   $('span.highlight').each(function(){ //удаляем старую подсветку
//    $(this).after($(this).html()).remove();  
//   });
//   var t = '';
//   $('.group .items').each(function(){ // в селекторе задаем область поиска


//    $(this).html($(this).html().replace(new RegExp(term, 'ig'), '<span class="highlight">$&</span>')); // выделяем найденные фрагменты
//    n = $('span.highlight').length; // количество найденных фрагментов
//    console.log('n = '+n);

//   });
//  }

// $('#searchfield').keyup(function(){
//   // if ($('#searchfield').val()!=term) // проверяем, изменилась ли строка
//   //  if ($('#searchfield').val().length>=minlen) { // проверяем длину строки
//     dosearch(); // если все в порядке, приступаем к поиску
//   //  }
//   //  else
//   //   $('#spresult').html(' '); // если строка короткая, убираем текст из DIVа с результатом 
//  });



var notfirst = false;
	var myMap;
	ymaps.ready(function () {
	    myMap = new ymaps.Map('map', {
	            center: [57.626569, 39.893787],
	            zoom: 14,
	            scroll: false,
	        }, {
	            searchControlProvider: 'yandex#search'
	        });

	        // Создаём макет содержимого.
	        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
	            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
	        ),

	        blueCollection = new ymaps.GeoObjectCollection(null, {
	                    preset: 'islands#blueIcon'
	                })

	        ost = new ymaps.GeoObjectCollection(null, {
	                    //preset: 'islands#redIcon'//bus-stop.png

                      iconLayout: 'default#image',
                      // Своё изображение иконки метки.
                      iconImageHref: '/bus-stop.png',
                      // Размеры метки.
                      iconImageSize: [24, 24],
                      // Смещение левого верхнего угла иконки относительно
                      // её "ножки" (точки привязки).
                      iconImageOffset: [-12, -12]
	                })




	        


	        

	        

	    
	});

  function render(argument) {


          MAP_OBJ.map(function(busitem) {
            
          
    
          $.post('request.php', { action: 'getroutecurve', type: busitem.cat, bort: busitem.id }, function(result)
              {

                var data = JSON.parse(result);
                  var s  = "";
                  blueCoords2 = [];
                  $.each(data, function(key, val)
                  {
                    //addPoint(lon,lat,title,azimut,img,ident,layr)
                    //addPoint(val[1], val[2], val[5], val[4], val[3], val[6], vt*10000+parseInt(val[0]), map.layers[vt+3]);

                    console.log(val);

                    blueCoords2.push([parseFloat(val[0]), parseFloat(val[1])]);



                  });


                  var myGeoObject = new ymaps.GeoObject({
                      // Описываем геометрию геообъекта.
                      geometry: {
                          // Тип геометрии - "Ломаная линия".
                          type: "LineString",
                          // Указываем координаты вершин ломаной.
                          coordinates: blueCoords2
                      },
                      // Описываем свойства геообъекта.
                      properties:{
                          // Содержимое хинта.
                          hintContent: "Я геообъект",
                          // Содержимое балуна.
                          balloonContent: "Меня можно перетащить"
                      }
                  }, {
                      // Задаем опции геообъекта.
                      // Включаем возможность перетаскивания ломаной.
                      //draggable: true,
                      // Цвет линии.
                      strokeColor: "#0800ff",
                      // Ширина линии.
                      strokeWidth: 5
                  });

                  myMap.geoObjects
                          .add(myGeoObject);

                  
                });

          
            $.post('request.php', { action: 'getroutestations', type: busitem.cat, bort: [busitem.id] }, function(result)
                {

                  var data = JSON.parse(result);
                  var s  = "";
                  blueCoords3 = [];
                  $.each(data, function(key, val)
                  {
                    //addPoint(lon,lat,title,azimut,img,ident,layr)
                    //addPoint(val[1], val[2], val[5], val[4], val[3], val[6], vt*10000+parseInt(val[0]), map.layers[vt+3]);

                    blueCoords3.push([parseFloat(val[1]), parseFloat(val[2])]);



                  });

                  

                  for (var i = 0, l = blueCoords3.length; i < l; i++) {

                          var tempPlacemark = new ymaps.Placemark(blueCoords3[i]);
                          tempPlacemark.events.add('click', function () {
                              alert('О, событие!');
                          });
                          ost.add(tempPlacemark);
                      }


                   myMap.geoObjects.add(ost);


                      


                  //myMap.geoObjects.remove(myPlacemark).add(myPlacemark);


                });


              

             


      });
  }

  getGEO();


	function getGEO() {


    var max = MAP_OBJ.length;

    var cur = 0;

    var dots = [];

    MAP_OBJ.map(function(busitem) {
		$.post('request.php', { action: 'getallroutes', type: busitem.cat, bort: [busitem.id] }, function(result)
        {

          var data = JSON.parse(result);

          cur++;

          data.map(function(e) {
            dots.push(e);
          })



          if(cur == max){
                      var s  = "";
                      blueCoords = [];
                      $.each(dots, function(key, val)
                      {
                        //addPoint(lon,lat,title,azimut,img,ident,layr)
                        //addPoint(val[1], val[2], val[5], val[4], val[3], val[6], vt*10000+parseInt(val[0]), map.layers[vt+3]);

                        blueCoords.push([parseFloat(val[2]), parseFloat(val[1]), val[3], val[4], val[0]]);
                        console.log(val);//check


                      });

                      blueCollection.each(function(index, el) {
                        console.log(index);
                      });

                      blueCollection.removeAll();

                      for (var i = 0, l = blueCoords.length; i < l; i++) {



                                //var img = rotateBase64Image(blueCoords[i][2], IMAGEEXAMPLE);

                                var tempPlacemark = new ymaps.Placemark([blueCoords[i][0], blueCoords[i][1]], {
                                    hintContent: 'Маршрут ' + blueCoords[i][3],
                                    balloonContent: 'Борт ' + blueCoords[i][4]
                                }, {
                                    // Опции.
                                    // Необходимо указать данный тип макета.
                                    iconLayout: 'default#image',
                                    // Своё изображение иконки метки.
                                    iconImageHref: '/image_generator/generate.php?a=' + blueCoords[i][2],
                                    // Размеры метки.
                                    iconImageSize: [32, 32],
                                    // Смещение левого верхнего угла иконки относительно
                                    // её "ножки" (точки привязки).
                                    iconImageOffset: [-16, -16]
                                });


                                tempPlacemark.events.add('click', function (item) {
                                    console.log(item);
                                    alert('О, событие! ');
                                });
                              
                              
                                blueCollection.add(tempPlacemark);

                              
                              
                          }

                          if(notfirst){
            //                  myMap.geoObjects.each(function (geoObject) {
                    //     if () {
                    //         // do something
                    //         ...
                    //         return false;
                    //     }
                    // });
                          }
                          else {
                            myMap.geoObjects.add(blueCollection);
                            notfirst = true;
                          }

          }


          
      	  
              


          //myMap.geoObjects.remove(myPlacemark).add(myPlacemark);

          
        });



  });

    setTimeout(function(){  //Beginning of code that should run AFTER the timeout
        getGEO();
        //lots more code
    },2500);  // put the timeout here
	}


  function rotateBase64Image(deg, base64data) {
      // var canvas = document.getElementById("c");
      // var ctx = canvas.getContext("2d");

      var context = cnxrender.getContext("2d");

      // context.canvas.width  = window.innerWidth;
      // context.canvas.height = window.innerHeight;

      // if (window.devicePixelRatio > 1) {
      //     var canvasWidth = cnxrender.width;
      //     var canvasHeight = cnxrender.height;

      //     cnxrender.width = canvasWidth * window.devicePixelRatio;
      //     cnxrender.height = canvasHeight * window.devicePixelRatio;
      //     cnxrender.style.width = canvasWidth;
      //     cnxrender.style.height = canvasHeight;

      //     context.scale(window.devicePixelRatio, window.devicePixelRatio);
      // }

      cnxrender.width  = 100;
      cnxrender.height = 100;

      context.clearRect(0, 0, cnxrender.width, cnxrender.height);

      //context.save();

      var image = new Image();
      image.src = base64data;
      
      context.translate(image.width/2, image.height/2);
      context.rotate((deg-90)*Math.PI/180);
      context.translate(-image.width/2, -image.height/2);

      context.drawImage(image, 0, 0);
      return cnxrender.toDataURL();
      

  } 

	//myMap.behaviors.disable('scrollZoom');