<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Max-Age: 1000");
header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding");
header("Access-Control-Allow-Methods: PUT, POST, GET, OPTIONS, DELETE");


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>tr76</title>
	<link rel="stylesheet" href="css/main.css">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">



	<script   src="https://code.jquery.com/jquery-3.2.1.min.js"   integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="   crossorigin="anonymous"></script> 
	<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>
<body>
	<div class="main-app">
		<div id="map"></div>
		<canvas id="cnxrender"/>
	</div>
	<div class="overlay-bl">
		<div class="search">
			<input type="text" id="searchfield" placeholder="Поиск" disabled="true">
		</div>
		<div class="items">
			<div class="group group-autobus">
				<div class="name">Автобусы</div>
				<div class="list"></div>
			</div>

			<div class="group group-trallbus">
				<div class="name">Троллейбусы</div>
				<div class="list"></div>
			</div>

			<div class="group group-tramv">
				<div class="name">Трамваи</div>
				<div class="list"></div>
			</div>

			<div class="group group-marsh">
				<div class="name">Маршрутки</div>
				<div class="list"></div>
			</div>
		</div>
		<a href="#" class="getMap">Показать на карте</a>
	</div>
</body>

<script src="js/qs.js"></script>
<script src="js/main.js"></script>
</html>